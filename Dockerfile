#########################
# Create base container #
#########################
FROM ubuntu:24.04 as base
LABEL maintainer="pidydx"

# Base setup
ENV BASE_PKGS dovecot-gssapi dovecot-imapd dovecot-ldap dovecot-lmtpd dovecot-managesieved

# Update users and groups
RUN userdel ubuntu
RUN groupadd -g 666 dovenull \
 && useradd -r -M -N -u 666 dovenull -g dovenull -s /usr/sbin/nologin

RUN groupadd -g 999 dovecot \
 && useradd -r -M -N -u 999 dovecot -g dovecot -s /usr/sbin/nologin

RUN groupadd -g 1000 vmail \
 && useradd -M -N -u 1000 vmail -g vmail -d /var/lib/vmail -s /usr/sbin/nologin

# Update and install base packages
RUN apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get upgrade -yq \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ca-certificates \
 && apt-get update -q \
 && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends ${BASE_PKGS} \
 && rm -rf /var/lib/apt/lists/*


##########################
# Create final container #
##########################
FROM base

# Prepare container
COPY etc/ /etc/
COPY usr/ /usr/

EXPOSE 24/tcp 993/tcp 4190/tcp 9993/tcp 12345/tcp
VOLUME ["/etc/dovecot", "/var/lib/vmail"]

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["dovecot"]
