#!/bin/bash

set -e

if [ "$1" = 'dovecot' ]; then
    exec /usr/sbin/dovecot -F
fi

exec "$@"